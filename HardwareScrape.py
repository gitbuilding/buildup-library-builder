#!/usr/bin/env python
from urllib import request as req
import re
import yaml
import os

wf_url = 'https://www.westfieldfasteners.co.uk/'
map_url = 'Product_Sitemap.html'
html = req.urlopen(wf_url+map_url).read().decode("unicode_escape")
all_products = re.findall('''<a class="link1_reference" href='([^']*)'>(.*)<\/a>''',html)

def find_items(product):
    link= product[0]
    part_html = req.urlopen(wf_url+link).read().decode("unicode_escape")
    #[0]: Link
    #[1]: Name
    #[2]: Product number
    return re.findall('''<td class="description_page_3"><a class="description_page_3" href="([^"]*)">(.*?)<br><span style="font-size:11px">\(([^)]*)\)''',part_html)

def replace_all(string,replacements):
    for replacement in replacements:
        string = string.replace(replacement[0],replacement[1])
    return string

categories = ['Full Hex Nuts Standard Pitch - ISO 4032 (DIN 934) in A2 Stainless Steel',
              'Full Hex Nuts Standard Pitch - ISO 4032 (DIN 934) in Brass',
              'Form A Washers ISO 7089 non chamfer & 7090 chamfered (DIN 125A) in A2 Stainless Steel'
             ]

re_categories = ['Hex Head Setscrews ISO 4017 \(DIN 933\) M[0-9\.]+ in A2 Stainless Steel',
                 'Socket Head Cap Screws ISO 4762 \(DIN 912\) M[0-9\.]+ in A2 Stainless Steel',
                 'Socket Head Button Screws ISO 7380 part 1 M[0-9\.]+ in A2 Stainless Steel'
                ]

replacements = [[' in A2 Stainless','_SS'],
                [' in Brass','_Brass'],
                ['Form A Washer ','Washer_'],
                ['Hex Nut ','Nut_'],
                ['Hex Bolt Full Thread ','HexBolt_'],
                ['Socket Head Button (Dome) Screw ','ButtonScrew_'],
                ['Socket Head Cap (Allen) Screw ','CapScrew_'],
                [' ','']
               ]

all_items={}
for product in all_products:
    items = []
    if product[1] in categories:
        items = find_items(product)
    else:
        for regex in re_categories:
            if re.match(regex, product[1]) is not None:
                items = find_items(product)
                break
    standards = re.findall('((?:ISO|DIN) [^ )]*)',product[1])
    for item in items:
        name = item[1]
        thread = re.findall('M[0-9\.]+',name)
        length = re.findall('M[0-9\.]+ x ([^ ]*)',name)
        shortname = replace_all(name,replacements)
        all_items[shortname] = {'Name':item[1],'Description':product[1],'Specs':{},'Suppliers':{'Westfield Fasteners':{'PartNo':item[2],'Link':wf_url+item[0]}}}
        if len(standards):
            for n,std in enumerate(standards):
                if len(standards) == 1:
                    mmc_name = 'McMaster Carr'
                else:
                    mmc_name = f'McMaster Carr ({n})'
                all_items[shortname]['Suppliers'][mmc_name] = {'PartNo': f'Search for {std}','Link':f'https://www.mcmaster.com/{std.replace(" ","-")}'}
            standards_str = str(standards)[1:-1]
            standards_str = standards_str.replace("'",'')
            all_items[shortname]['Specs']['Standards'] = standards_str
        if len(thread) ==1:
            all_items[shortname]['Specs']['Thread'] = thread[0]
        if len(length) ==1:
            all_items[shortname]['Specs']['Length'] = length[0]
        if "in Brass" in name:
            all_items[shortname]['Specs']['Material'] = 'Brass'
        if "in A2 Stainless" in name:
            all_items[shortname]['Specs']['Material'] = 'A2 Stainless Steel (Also known as: SAE 304; 18-8; UNS S30400; SUS304; European norm 1.4301)'

if not os.path.exists('Output'):
    os.makedirs('Output')
with open(os.path.join('Output','Hardware.yaml'), 'w') as yaml_file:
    yaml_file.write(yaml.dump(all_items)) 