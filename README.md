# BuildUp library builder

This repository is for scripts to automatically build [BuildUp libraries](https://gitlab.com/bath_open_instrumentation_group/git-building/blob/master/BuildUpLibrary.md) for [Git-Building](https://gitlab.com/bath_open_instrumentation_group/git-building).

Currently only containts one script that build a basic hardware library with just one supplier.
