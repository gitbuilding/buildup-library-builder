from urllib import request as req
import xml.etree.ElementTree as ET
import re

url = 'https://www.accu.co.uk/sitemap.xml'
xml = req.urlopen(url).read().decode("unicode_escape")

root = ET.fromstring(xml)
ns = {'root':re.findall('\{([^}]*)\}',root.tag)[-1]}


sitemaps = []
for sitemap in root.findall('root:sitemap',ns):
    loc = sitemap.find('root:loc',ns)
    sitemaps.append(loc.text)


parts = []
for sitemap in sitemaps:
    xml = req.urlopen(sitemap).read().decode("unicode_escape")
    root = ET.fromstring(xml)

    ns_xml = re.findall('xmlns(?:\:([a-zA-Z0-9]*))?="([^\n"]*)"',xml[:1000])
    ns = {}
    for name in ns_xml:
        if name[0]:
            ns[name[0]]=name[1]
        else:
            ns['root']=name[1]

    for pageurl in root.findall('root:url',ns):
        im = pageurl.find('image:image',ns)
        if im is not None:
            title = im.find('image:title',ns)
            if title is not None:
                parts.append([title.text,pageurl.find('root:loc',ns).text])